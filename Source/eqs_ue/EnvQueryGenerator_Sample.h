// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryGenerator.h"
#include "EnvQueryGenerator_Sample.generated.h"

/**
 * 
 */
UCLASS()
class EQS_UE_API UEnvQueryGenerator_Sample : public UEnvQueryGenerator
{
	GENERATED_BODY()

	UEnvQueryGenerator_Sample(const FObjectInitializer& ObjectInitializer);

	virtual void GenerateItems(FEnvQueryInstance& QueryInstance) const override;
};
