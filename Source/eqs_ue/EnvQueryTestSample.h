// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTest.h"
#include "EnvQueryTestSample.generated.h"

/**
 * 
 */
UCLASS()
class EQS_UE_API UEnvQueryTestSample : public UEnvQueryTest
{
	GENERATED_BODY()

	UEnvQueryTestSample(const FObjectInitializer& ObjectInitializer);

	virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;
};
