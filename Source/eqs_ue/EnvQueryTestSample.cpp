// Fill out your copyright notice in the Description page of Project Settings.


#include "EnvQueryTestSample.h"

#include "EnvironmentQuery/Items/EnvQueryItemType_ActorBase.h"

UEnvQueryTestSample::UEnvQueryTestSample(const FObjectInitializer& ObjectInitializer)
{
	Cost = EEnvTestCost::Low;

	SetWorkOnFloatValues(false);

	ValidItemType = UEnvQueryItemType_ActorBase::StaticClass();
	
}

void UEnvQueryTestSample::RunTest(FEnvQueryInstance& QueryInstance) const
{
	//todo: implementation
	//iterate all items, filter and give a score.

	//try empty all items

	QueryInstance.Items.Empty();
}
