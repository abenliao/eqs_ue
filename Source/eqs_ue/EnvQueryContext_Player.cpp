// Fill out your copyright notice in the Description page of Project Settings.


#include "EnvQueryContext_Player.h"

#include "EnvironmentQuery/EnvQueryTypes.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Point.h"

void UEnvQueryContext_Player::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
	AActor* QueryOwner = Cast<AActor>(QueryInstance.Owner.Get());

	//FVector Loc = QueryOwner->GetActorLocation();

	//FVector Loc = {-750,390,226};

	UWorld* World = QueryOwner->GetWorld();

	if(World != nullptr)
	{
		if(World->GetFirstPlayerController() != nullptr)
		{
			if(World->GetFirstPlayerController()->GetPawn() != nullptr)
			{
				FVector Loc = World->GetFirstPlayerController()->GetPawn()->GetActorLocation();
				UEnvQueryItemType_Point::SetContextHelper(ContextData, Loc);
			}
		}
	}
}
