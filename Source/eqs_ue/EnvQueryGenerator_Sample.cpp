// Fill out your copyright notice in the Description page of Project Settings.


#include "EnvQueryGenerator_Sample.h"

#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Point.h"

UEnvQueryGenerator_Sample::UEnvQueryGenerator_Sample(const FObjectInitializer& ObjectInitializer)
{
	ItemType = UEnvQueryItemType_Actor::StaticClass();
}

void UEnvQueryGenerator_Sample::GenerateItems(FEnvQueryInstance& QueryInstance) const
{
	AActor* QueryOwner = Cast<AActor>(QueryInstance.Owner.Get());
    
    UWorld* World = QueryOwner->GetWorld();

    if(World != nullptr)
    {
    	if(World->GetFirstPlayerController() != nullptr)
    	{
    		if(World->GetFirstPlayerController()->GetPawn() != nullptr)
    		{
    			AActor *Actor = World->GetFirstPlayerController()->GetPawn();
				QueryInstance.AddItemData<UEnvQueryItemType_Actor>(Actor);
    		}
    	}
    }
}
